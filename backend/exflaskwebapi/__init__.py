import flask

from exflaskwebapi import api


app = flask.Flask(__name__)
app.register_blueprint(api.bp)
