import json
import os

import flask
from flask import request

bp = flask.Blueprint("api", __name__)


@bp.route("/")
def root():
    return "Hello, World!"


@bp.route("/random")
def get_random():
    num_values = request.args.get("n", 1, int)
    values = [f"{n:02x}" for n in os.urandom(num_values)]
    resp = flask.Response(
        json.dumps({"result": values}),
        200
    )
    resp.set_etag("Cache-Control", "no-cache")
    resp.content_type = "application/json"
    return resp
