import flask
import flask.testing
import pytest

import exflaskwebapi


@pytest.fixture
def client():
    with exflaskwebapi.app.test_client() as client:
        with exflaskwebapi.app.app_context():
            pass
        yield client


def test_foo(client: flask.testing.FlaskClient):
    resp: flask.Response = client.get("/")
    assert resp.status_code == 200
    assert resp.get_data(as_text=True) == "Hello, World!"


@pytest.mark.parametrize("n", [0, 1, 2])
def test_random(client: flask.testing.FlaskClient, n: int):
    resp: flask.Response = client.get(f"/random?n={n}")
    assert resp.status_code == 200
    assert resp.is_json
    data = resp.get_json()
    assert "result" in data
    assert isinstance(data["result"], list)
    assert len(data["result"]) == n
