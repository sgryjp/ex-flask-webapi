import React from "react";

import Alert from "antd/lib/alert";
import Form from "antd/lib/form";
import InputNumber from "antd/lib/input-number";
import Layout from "antd/lib/layout";
import Table from "antd/lib/table";
import Typography from "antd/lib/typography";
import "antd/dist/antd.css";

const { Text, Title } = Typography;

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage: "",
      numRandomNumbers: 0
    };
    this.handleNumRandomNumbers = this.handleNumRandomNumbers.bind(this);
  }

  async componentDidUpdate(prevProps, prevState) {
    try {
      if (prevState.numRandomNumbers !== this.state.numRandomNumbers && 0 < this.state.numRandomNumbers) {
        const uri = `http://localhost:8080/random?n=${this.state.numRandomNumbers}`;
        const resp = await fetch(uri).catch(err => {
          throw Error("Failed to fetch the data: " + err.message);
        });
        console.log(resp);
        const numbers = await resp.text();
        console.log(numbers);
      }
    }
    catch (error) {
      this.setState({ errorMessage: error.message });
    }
  }

  handleNumRandomNumbers(newValue) {
    this.setState({ numRandomNumbers: newValue });
  }

  render() {
    return (
      <div>
        <Layout.Header theme="light">
          <Text style={{ color: "#fff" }}>ex-flask-webapi</Text>
        </Layout.Header>
        <Layout.Content style={{ paddingLeft: 64, paddingRight: 64 }}>
          <section>
            <Title level={1}>Parameters</Title>
            <Form>
              <Form.Item label="Number of random values" htmlFor="num-randoms">
                <InputNumber
                  id="num-randoms"
                  value={this.state.numRandomNumbers}
                  onChange={this.handleNumRandomNumbers}
                />
              </Form.Item>
            </Form>
          </section>
          <section>
            <Title level={1}>Result</Title>
            {this.state.errorMessage
              ? <Alert type="error" message={this.state.errorMessage} />
              : null}
            <Table />
          </section>
        </Layout.Content>
      </div>
    );
  }
}
